# Fewer Cores, More Hertz: Leveraging High-Frequency Cores in the OS Scheduler for Improved Application Performance

**Authors:** Redha Gouicem, Damien Carver, Jean-Pierre Lozi, Baptiste Lepers, Willy Zwaenepoel, Julien Sopena, Nicolas Palix, Julia Lawall, Gilles Muller

This repository contains the two patches proposed in the paper (add link when available here).
Both patches were tested against a Linux 5.4 kernel.
You can apply them with this command, from the root of a kernel source tree:

``` bash
patch -p1 < 5.4-Smove.patch
```

The same goes for `5.4-Slocal.patch`.

**Note on 5.4-Smove.patch:** When using this patch, you will need to setup the frequency threshold by hand at runtime. To do so, you must write to value in kHz in `/proc/sys/kernel/sched_lowfreq`.
A good value is the minimal freuqency of your processor. You can use it with this command:

``` bash
cat /sys/devices/system/cpu/cpufreq/policy0/cpuinfo_min_freq | sudo tee /proc/sys/kernel/sched_lowfreq
```
